!ifndef VERSION
  !define VERSION '15.09.80'
!endif

; The name of the installer
Name "Dremel3DSlicer ${VERSION}"

; The file to write
OutFile "Dremel3DSlicer_${VERSION}.exe"

; The default installation directory
; Dremel3DSlicer_${VERSION}
InstallDir $PROGRAMFILES\Dremel DigiLab 3D Slicer_${VERSION}

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\Dremel3DSlicer_${VERSION}" "Install_Dir"

; Request application privileges for Windows Vista
RequestExecutionLevel admin

; Set the LZMA compressor to reduce size.
SetCompressor /SOLID lzma
;--------------------------------

!include "MUI2.nsh"
!include "Library.nsh"

; !define MUI_ICON "dist/resources/cura.ico"
!define MUI_BGCOLOR FFFFFF

; Directory page defines
!define MUI_DIRECTORYPAGE_VERIFYONLEAVE

; Header
; Don't show the component description box
!define MUI_COMPONENTSPAGE_NODESC

;Do not leave (Un)Installer page automaticly
!define MUI_FINISHPAGE_NOAUTOCLOSE
!define MUI_UNFINISHPAGE_NOAUTOCLOSE

;Run Dremel3DSlicer after installing
!define MUI_FINISHPAGE_RUN
!define MUI_FINISHPAGE_RUN_TEXT "Start Dremel DigiLab 3D Slicer ${VERSION}"
!define MUI_FINISHPAGE_RUN_FUNCTION "LaunchLink"

;Add an option to show release notes
!define MUI_FINISHPAGE_SHOWREADME "$INSTDIR\plugins\ChangeLogPlugin\changelog.txt"

; Pages
;!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH

; Languages
!insertmacro MUI_LANGUAGE "English"

; Reserve Files
!insertmacro MUI_RESERVEFILE_LANGDLL
ReserveFile '${NSISDIR}\Plugins\InstallOptions.dll'

;--------------------------------

; The stuff to install
Section "Dremel3DSlicer ${VERSION}"

  SectionIn RO
  
  ; Set output path to the installation directory.
  SetOutPath $INSTDIR
  
  ; Put file there
  File /r "dist\"
  
  ; Write the installation path into the registry
  WriteRegStr HKLM "SOFTWARE\Dremel3DSlicer_${VERSION}" "Install_Dir" "$INSTDIR"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Dremel3DSlicer_${VERSION}" "DisplayName" "Dremel3DSlicer ${VERSION}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Dremel3DSlicer_${VERSION}" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Dremel3DSlicer_${VERSION}" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Dremel3DSlicer_${VERSION}" "NoRepair" 1
  WriteUninstaller "uninstall.exe"

  ; Write start menu entries for all users
  SetShellVarContext all
  
  CreateDirectory "$SMPROGRAMS\Dremel3DSlicer ${VERSION}"
  CreateShortCut "$SMPROGRAMS\Dremel3DSlicer ${VERSION}\Uninstall Dremel3DSlicer ${VERSION}.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
  CreateShortCut "$SMPROGRAMS\Dremel3DSlicer ${VERSION}\Dremel Dremel3DSlicer ${VERSION}.lnk" "$INSTDIR\Dremel3DSlicer.exe" '' "$INSTDIR\Dremel3DSlicer.exe" 0
  
SectionEnd

Function LaunchLink
  ; Write start menu entries for all users
  SetShellVarContext all
  Exec '"$WINDIR\explorer.exe" "$SMPROGRAMS\Dremel3DSlicer ${VERSION}\Dremel3DSlicer ${VERSION}.lnk"'
FunctionEnd

Section "Install Visual Studio 2010 Redistributable"
    SetOutPath "$INSTDIR"
    File "vcredist_2010_20110908_x86.exe"
    
    IfSilent +2
      ExecWait '"$INSTDIR\vcredist_2010_20110908_x86.exe" /q /norestart'

SectionEnd

Section "Install Arduino Drivers"
  ; Set output path to the driver directory.
  SetOutPath "$INSTDIR\drivers\"
  File /r "drivers\"
  
  ${If} ${RunningX64}
    IfSilent +2
      ExecWait '"$INSTDIR\drivers\dpinst64.exe" /lm'
  ${Else}
    IfSilent +2
      ExecWait '"$INSTDIR\drivers\dpinst32.exe" /lm'
  ${EndIf}
SectionEnd

Section "Open STL files with Dremel3DSlicer"
	WriteRegStr HKCR .stl "" "Dremel3DSlicer STL model file"
	DeleteRegValue HKCR .stl "Content Type"
	WriteRegStr HKCR "Dremel3DSlicer STL model file\DefaultIcon" "" "$INSTDIR\Dremel3DSlicer.exe,0"
	WriteRegStr HKCR "Dremel3DSlicer STL model file\shell" "" "open"
	WriteRegStr HKCR "Dremel3DSlicer STL model file\shell\open\command" "" '"$INSTDIR\Dremel3DSlicer.exe" "%1"'
SectionEnd

Section /o "Open OBJ files with Dremel3DSlicer"
	WriteRegStr HKCR .obj "" "Dremel3DSlicer OBJ model file"
	DeleteRegValue HKCR .obj "Content Type"
	WriteRegStr HKCR "Dremel3DSlicer OBJ model file\DefaultIcon" "" "$INSTDIR\Dremel3DSlicer.exe,0"
	WriteRegStr HKCR "Dremel3DSlicer OBJ model file\shell" "" "open"
	WriteRegStr HKCR "Dremel3DSlicer OBJ model file\shell\open\command" "" '"$INSTDIR\Dremel3DSlicer.exe" "%1"'
SectionEnd

;--------------------------------

; Uninstaller

Section "Uninstall"
  
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Dremel3DSlicer_${VERSION}"
  DeleteRegKey HKLM "SOFTWARE\Dremel3DSlicer_${VERSION}"

  ; Write start menu entries for all users
  SetShellVarContext all
  ; Remove directories used
  RMDir /r "$SMPROGRAMS\Dremel3DSlicer ${VERSION}"
  RMDir /r "$INSTDIR"

SectionEnd
